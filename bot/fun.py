from pyrogram import Client, filters


@Client.on_message(filters.command("hug"))
def hug(client, message):
    if message.reply_to_message:
        message.reply_to_message.reply_text("**Von mir auch**")
    else:
        message.reply_to_message("**Ich dich auch**")
