from pyrogram import Client, filters
from db_models import TelegramUser


@Client.on_message(filters.command(["set_title", "set_nick"]))
def set_title(client, message):
    if message.from_user.id not in [user.user_id for user in TelegramUser.select()]:
        TelegramUser.create(user_id=message.from_user.id)
    user = TelegramUser.get(TelegramUser.user_id == message.from_user.id)
    user.title = " ".join(message.command[1:])
    user.save()
    # TODO: promote user in all groups. if in private chat.
    # TODO: automatically promote user if he sends message in a chat where he isn't promoted yet.
    if message.chat.type != "private":
        if (
            (
                message.from_user.id
                not in [
                    user.user.id
                    for user in client.iter_chat_members(
                        message.chat.id, filter="administrators"
                    )
                ]
                and len(
                    list(
                        client.iter_chat_members(
                            message.chat.id, filter="administrators"
                        )
                    )
                )
                < 50
            )
            or client.get_chat_member(
                chat_id=message.chat.id, user_id=message.from_user.id
            ).status
            == "administrator"
        ):
            message.chat.promote_member(
                message.from_user.id,
                can_change_info=False,
                can_delete_messages=False,
                can_invite_users=True,
                can_pin_messages=False,
                can_promote_members=False,
                can_restrict_members=False,
            )
            client.set_administrator_title(
                message.chat.id,
                message.from_user.id,
                " ".join(message.command[1:])
                if len(user.title) <= 16
                else user.title[: 16 - 3] + "...",
            )
        elif message.from_user.id in [
            user.user.id
            for user in client.iter_chat_members(
                message.chat.id, filter="administrators"
            )
        ]:
            client.set_administrator_title(
                message.chat.id,
                message.from_user.id,
                user.title if len(user.title) <= 16 else user.title[: 16 - 3] + "...",
            )
    message.reply_text("Klaro")
