import time
from pyrogram import Client, filters
from db_models import TelegramGroup


@Client.on_message(filters.new_chat_members)
def welcome(client, message):
    if message.chat.id not in [group.group_id for group in TelegramGroup.select()]:
        return
    welcome_msg = TelegramGroup.get(TelegramGroup.group_id == message.chat.id).welcome
    if welcome_msg:
        if message.chat.username == "asozialchat":
            for member in message.new_chat_members:
                print("ja, es ist der asozialste chat")
                if member in client.get_chat("asozial").iter_chat_members():
                    message.reply_text(
                        welcome_msg.format(first_name=message.from_user.first_name)
                    )
                else:
                    client.kick_chat_member(
                        message.chat.id, member.id, time.time() + 60
                    )
        else:
            message.reply_text(
                welcome_msg.format(first_name=message.from_user.first_name)
            )


@Client.on_message(filters.command("set_welcome"))
def set_welcome(client, message):
    if message.chat.id in [group.group_id for group in TelegramGroup.select()]:
        group = TelegramGroup.get(TelegramGroup.group_id == message.chat.id)
        group.welcome = message.text.replace("/set_welcome", "")
        group.save()
        message.reply_text(
            "Ab jetzt werde ich Menschen mit der Nachricht begrüßen. Ich freu mich schon!"
        )
    else:
        message.reply_text("Du bist noch nicht zugelassen")
