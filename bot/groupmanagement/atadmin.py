from pyrogram import Client, filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton


@Client.on_message(filters.regex("@admin"))
def atadmin(client, message):
    msg = message.reply_to_message if message.reply_to_message else message
    for admin in message.chat.iter_members():
        if admin.can_delete_messages or admin.status == "creator":
            try:
                client.send_message(
                    admin.user.id,
                    f"Jemand braucht Dich in {message.chat.title} wegen"
                    f" [{msg.from_user.first_name}](tg://user?id={msg.from_user.id}): ```{msg.text}```",
                    reply_markup=InlineKeyboardMarkup(
                        [
                            [
                                InlineKeyboardButton(
                                    "Gehe zur Nachricht",
                                    url=f"t.me/c/{str(msg.chat.id).replace('-100', '')}/{msg.message_id}",
                                )
                            ]
                        ]
                    ),
                )
            except Exception as e:
                print(e)
    msg.reply_text("Jetzt kommt die Polizei und knüppelt euch nieder...")
