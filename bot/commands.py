from pyrogram import Client, filters

print("loaded start")


@Client.on_message(filters.command(["start", "start@asoziales_netzwerk_bot"]))
def start(client, message):
    print("start")
    message.reply_text(
        "Hallo, ich bin der Bot des Asozialen Netzwerks! Falls du wissen möchtest was ich kann, schreib"
        " /help"
    )


@Client.on_message(filters.command(["help", "help@asoziales_netzwerk_bot"]))
def help(client, message):
    if message.chat.type in ("group", "supergroup"):
        chat_member = client.get_chat_member(message.chat.id, message.from_user.id)
    else:
        chat_member = None
    commands = list()
    commands.append(
        "/set_title `[dein titel]` setze deinen natürlich unbedeutenden Rang"
    )
    commands.append("/contact Kontaktiere jemanden vom Asozialen Netzwerk")
    if chat_member and (
        chat_member.can_restrict_members or chat_member.status == "creator"
    ):
        commands.append(
            "/add_contact `[wer ihr seid] | [beschreibung, was ihr macht]` Diese Gruppe als Kontaktgruppe "
            "hinzufügen, damit man über mich mit euch schreiben kann."
        )
        commands.append("/set_welcome `[willkommens nachricht]` Begrüße")
    for admin in client.iter_chat_members("asozial", filter="administrators"):
        if admin.can_post_messages and admin.user.id == message.from_user.id:
            commands.append(
                "/create_quiz `frage {dauer, z.B. 1d oder 5h} | [Antwort 1] {[r, wenn richtig, f wenn "
                "falsch]} | [antwort 2] {[r, wenn richtig, f, wenn falsch]} | ...` Erstelle ein Quiz"
            )
    commands.append(
        f"\n\n ```Das sind nur die Commands, die hier, von {message.from_user.first_name} "
        "benutzt werden können.```"
    )
    message.reply_text("\n".join(commands))
