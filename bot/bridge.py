import requests
from pyrogram import Client, filters
from discord import Webhook, RequestsWebhookAdapter
from db_models import TelegramGroup, Bridge, DiscordChannel, TelegramUser
from custom_filter import group_allowed


def upload(file):
    with open(file, "rb") as f:
        return (
            "https://telegra.ph"
            + requests.post(
                "https://telegra.ph/upload", files={"file": ("file", f, "image/png")}
            ).json()[0]["src"]
        )


@Client.on_message(filters.command("set_discord_bridge"))
def setup_discord_bridge(client, message):
    webhook = Webhook.partial(
        int(message.command[1]), message.command[2], adapter=RequestsWebhookAdapter()
    )
    print(webhook.guild_id)
    discord = DiscordChannel.create(
        guild=int(message.command[3]),
        channel=int(message.command[4]),
        webhook_id=int(message.command[1]),
        webhook_token=message.command[2],
    )

    telegram = TelegramGroup.get(TelegramGroup.group_id == message.chat.id)
    Bridge.create(discord=discord, telegram=telegram)
    webhook.send("Hierher wird jetzt von Telegram gebridged.", username="Bridgebot")
    message.reply_text("Ab jetzt wird von hier nach Discord gebridged.")
    message.delete()


@Client.on_message(group_allowed & filters.group & filters.incoming, group=-1)
def bridge_from_telegram(client, message):
    tggroup = TelegramGroup.get(TelegramGroup.group_id == message.chat.id)
    for bridge in Bridge.select():
        if (bridge.telegram == tggroup) and (bridge.discord is not None):

            pp = client.get_profile_photos(message.from_user.id)
            pp = pp[0] if pp else None
            pp_url = upload(client.download_media(pp)) if pp else None
            webhook = Webhook.partial(
                bridge.discord.webhook_id,
                bridge.discord.webhook_token,
                adapter=RequestsWebhookAdapter(),
            )

            title = None
            if message.from_user.id in [
                telegram.user_id for telegram in TelegramUser.select()
            ]:
                title = TelegramUser.get(
                    TelegramUser.user_id == message.from_user.id
                ).title
            if ("jo" not in message.text.lower()) or len(message.text) > 5:
                webhook.send(
                    message.text,
                    username=(title if title else message.from_user.first_name)
                    + " [TG]",
                    avatar_url=pp_url,
                )
