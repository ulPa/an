from datetime import datetime, timedelta, time
import locale
from pyrogram import Client, filters
from apscheduler.schedulers.background import BackgroundScheduler

locale.setlocale(locale.LC_TIME, "de_DE.UTF-8")
scheduler = BackgroundScheduler()


@Client.on_message(filters.command("tk"))
def ask_conference(client, message):
    poll = client.send_poll(
        chat_id=message.chat.id,
        question=f"An welchem Tag könnt ihr an einer Telefonkonferenz zum Thema {' '.join(message.command[1:])} teilnehmen?"
        if len(message.command) != 1
        else "An welchen Tag könnt ihr an einer Telefonkonferenz teilnehmen?",
        options=[
            datetime.date(datetime.today() + timedelta(days=d)).strftime("%A, %d.%m.%y")
            for d in range(2, 12)
        ],
        allows_multiple_answers=True,
    )
    poll.pin()
    scheduler.add_job(
        ask_conference_time,
        "date",
        run_date=datetime.now() + timedelta(hours=12),
        args=[client, poll],
    )


def ask_conference_time(client, previous_poll):
    client.stop_poll(previous_poll.chat.id, previous_poll.message_id)
    previous_poll = client.get_messages(previous_poll.chat.id, previous_poll.message_id)
    mv_text = []
    mv_amount = 0
    for option in previous_poll.poll.options:
        if option.voter_count > mv_amount:
            mv_text = [option.text]
            mv_amount = option.voter_count
        elif option.voter_count == mv_amount:
            mv_text.append(option.text)
    if len(mv_text) > 1:
        poll = previous_poll.reply_poll(
            question=previous_poll.poll.question, options=mv_text,
        )
        scheduler.add_job(
            ask_conference_time,
            "date",
            run_date=datetime.now() + timedelta(hours=5),
            args=[client, poll],
        )
        poll.pin()
    else:
        poll = client.send_poll(
            chat_id=previous_poll.chat.id,
            question=f"Zu welcher Zeit könnt ihr am {mv_text[0]} eine TK machen?",
            options=[time(hour=d).strftime("%H:%M Uhr") for d in range(13, 23)],
            allows_multiple_answers=True,
        )
        scheduler.add_job(
            evaluate_time_poll,
            "date",
            run_date=datetime.now() + timedelta(hours=12),
            args=[client, poll, datetime.strptime(mv_text[0], "%A, %d.%m.%y")],
        )
        poll.pin()


def evaluate_time_poll(client, poll, tk_date):
    client.stop_poll(poll.chat.id, poll.message_id)
    poll = client.get_messages(poll.chat.id, poll.message_id)
    mv_time = []
    mv_votes = 0
    for option in poll.poll.options:
        if option.voter_count > mv_votes:
            mv_time = [option.text]
            mv_votes = option.voter_count
        elif option.voter_count == mv_votes:
            mv_time.append(option.text)
    if len(mv_time) > 1:
        poll = poll.reply_poll(question=poll.poll.question, options=mv_time,)
        scheduler.add_job(
            evaluate_time_poll,
            "date",
            run_date=datetime.now() + timedelta(hours=5),
            args=[client, poll, tk_date],
        )
        poll.pin()
    else:
        poll.reply_text(
            datetime(
                tk_date.year,
                tk_date.month,
                tk_date.day,
                datetime.strptime(mv_time[0], "%H:%M Uhr").hour,
            ).strftime(
                "Es findet am %A dem %d.%m.%y um %H:%M eine Telefonkonferenz statt."
            )
        )


scheduler.start()
