from uuid import uuid4
from pyrogram import Client, filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from db_models import TelegramGroup, TelegramInlineButton
from custom_filter import inline_button_filter

groups = list()


@Client.on_message(filters.group, group=-101)
def check_chat(client, message):
    if message.chat.id not in [group.group_id for group in TelegramGroup.select()] + [
        groups
    ]:
        message.reply_text("Diese Gruppe ist noch nicht Zugelassen.")
        groups.append(message.chat.id)
        keyboard = [
            [
                InlineKeyboardButton(
                    text="Zulassen",
                    callback_data=TelegramInlineButton.create(
                        data=f"gc:al:{message.chat.id}", button_id=str(uuid4())
                    ).button_id,
                )
            ],
            [
                InlineKeyboardButton(
                    text="Ablehnen",
                    callback_data=TelegramInlineButton.create(
                        data=f"gc:da:{message.chat.id}", button_id=str(uuid4())
                    ).button_id,
                )
            ],
        ]
        client.send_message(
            -368129848,
            f"**{message.chat.title}**\n"
            f"{message.chat.username}\n"
            f"{message.chat.description}",
            reply_markup=InlineKeyboardMarkup(keyboard),
        )
        print(message)


@Client.on_callback_query(inline_button_filter("gc:"))
def groupcheck_button(client, callback_query):
    data = TelegramInlineButton.get(button_id=callback_query.data).data
    if "al" in data:
        print("create group row")
        TelegramGroup.create(group_id=int(data.replace("gc:al:", "")))
        client.send_message(
            int(data.replace("gc:al:", "")),
            "Perfekt. Diese Gruppe ist jetzt zugelassen worden.",
        )
        callback_query.answer("Jo")
    elif "da" in data:
        client.send_message(
            int(data.replace("gc:da:", "")),
            "Diese Gruppe gehört nicht zum Asozialen Netzwerk.",
        )
        client.leave_chat(int(data.replace("gc:da:", "")))
        print("leaving group")
        callback_query.answer("Bin aus der Gruppe ausgetreten")
