import re
from uuid import uuid4
from time import sleep
from pyrogram import Client, filters
from pyrogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from db_models import TelegramContactGroup, TelegramInlineButton, TelegramGroup
from custom_filter import group_allowed, inline_button_filter


get_in_contact_filter = filters.create(
    lambda client, message: (
        message.reply_to_message.from_user
        and message.reply_to_message.from_user.id == 1089639460
    )
    and (re.compile(r"^\d+::.*").match(message.reply_to_message.text))
)
reply_contacter_filter = filters.create(
    lambda client, message: (message.reply_to_message.from_user.id == 1089639460)
    and (
        message.reply_to_message.forward_from
        or message.reply_to_message.forward_sender_name
    )
)


@Client.on_message(filters.command(["contact", "contact@asoziales_netzwerk_bot"]))
def contact(client, message, edit=False):
    keyboard = [
        [
            InlineKeyboardButton(
                text=group.name,
                callback_data=TelegramInlineButton.create(
                    data=f"ct:gr:{group._pk}", button_id=str(uuid4())
                ).button_id,
            )
        ]
        for group in sorted(
            list(TelegramContactGroup.select()), key=lambda group: group.name
        )
    ]
    if edit:
        message.edit_text(
            "Mit wem möchtest du Kontakt aufnehmen?",
            reply_markup=InlineKeyboardMarkup(keyboard),
        )
    else:
        message.reply_text(
            "Mit wem möchtest du Kontakt aufnehmen?",
            reply_markup=InlineKeyboardMarkup(keyboard),
        )


@Client.on_message(filters.command("add_contact") & filters.group & group_allowed)
def add_contact(client, message):
    if message.chat.id not in [
        group.group.group_id for group in TelegramContactGroup.select()
    ]:
        message.reply_text("OK. ich frage meinen Entwickler.")
        name, description = message.text.replace("/add_contact", "").split("|")
        keyboard = [
            [
                InlineKeyboardButton(
                    "Erlauben",
                    callback_data=TelegramInlineButton.create(
                        data=f"ct:al:{message.chat.id}:{name}:{description}",
                        button_id=str(uuid4()),
                    ).button_id,
                )
            ],
            [
                InlineKeyboardButton(
                    "Ablehnen",
                    callback_data=TelegramInlineButton.create(
                        data=f"ct:dn:{message.chat.id}:{name}:{description}",
                        button_id=str(uuid4()),
                    ).button_id,
                )
            ],
        ]
        client.send_message(
            text=f"**{name}** aus {message.chat.title} \n"
            f"`{description}`\n\n"
            f"Als kontaktgruppe hinzufügen?",
            chat_id=-368129848,
            reply_markup=InlineKeyboardMarkup(keyboard),
        )
    else:
        message.reply_text("Diese Gruppe ist schon eine Kontaktgruppe")


@Client.on_callback_query(inline_button_filter("ct:"))
def allow_deny_request(client, query):
    data = TelegramInlineButton.get(query.data == TelegramInlineButton.button_id).data
    if "al" in data:
        chat, name, description = data.replace("ct:al:", "").split(":")
        TelegramContactGroup.create(
            group=TelegramGroup.get(TelegramGroup.group_id == int(chat)),
            name=name,
            description=description,
        )
        client.send_message(
            chat_id=int(chat),
            text="Eure Gruppe wurde nun als Kontaktgruppe hinzugefügt, also kann man"
            " jetzt über mich mit euch Kontakt aufnehmen.",
        )
        query.answer("Jo")
    elif "dn" in data:
        chat, name, description = data.replace("ct:dn:", "").split(":")
        client.send_message(
            chat_id=int(chat),
            text="Diese Gruppe wurde leider nicht als Kontaktgruppe hinzugefügt.",
        )
        query.answer("Jo")
    elif "gr" in data:
        chat = data.replace("ct:gr:", "")
        cntctgrp = TelegramContactGroup.get_by_id(int(chat))
        query.message.edit_text(
            f"{chat}:: **{cntctgrp.name}** \n"
            f"{cntctgrp.description} \n\n"
            f"`Antworte auf diese Nachricht um Kontakt aufzunehmen`",
            reply_markup=InlineKeyboardMarkup(
                [
                    [
                        InlineKeyboardButton(
                            "Zurück",
                            callback_data=TelegramInlineButton.create(
                                data="ct:bc", button_id=str(uuid4())
                            ).button_id,
                        )
                    ]
                ]
            ),
        )
    elif "bc" in data:
        contact(client, query.message, edit=True)


@Client.on_message(filters.reply & get_in_contact_filter)
def get_in_contact(client, message):
    cgroup_id = re.findall(r"^(\d+)::.*", message.reply_to_message.text)[0]
    chat_id = TelegramContactGroup.get_by_id(int(cgroup_id)).group.group_id
    if message.chat.type is not "private":
        name = message.chat.title
    else:
        name = message.chat.first_name
    client.send_message(
        chat_id, f"**{name}** (@{message.chat.username},{message.chat.id})",
    )
    message.forward(TelegramContactGroup.get_by_id(int(cgroup_id)).group.group_id)
    msg = message.reply_text(
        f"Nachricht wurde an {TelegramContactGroup.get_by_id(int(cgroup_id)).name} gesendet"
    )
    sleep(10)
    msg.delete()


@Client.on_message(filters.reply & reply_contacter_filter)
def reply_to_contacter(client, message):
    chat_id = (
        int(
            re.findall(
                r".+\(@\w+,(-?\d+)\)",
                client.get_messages(
                    message.chat.id, message.reply_to_message.message_id - 1
                ).text,
            )[0]
        )
        if not message.forward_from
        else message.forward_from.id
    )
    for group in TelegramContactGroup.select():
        if group.group.group_id == message.chat.id:
            contactgroup = group
    client.send_message(
        chat_id,
        f"{contactgroup._pk}:: **{contactgroup.name}** schrieb:\n\n" f"{message.text}",
    )
    chat = client.get_chat(chat_id)
    msg = message.reply_text(
        f"Nachricht an {chat.first_name if chat.first_name else chat.title} Weitergeleitet."
    )
    sleep(10)
    msg.delete()
