import re
from uuid import uuid4
from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from pyrogram import Client, filters
from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from interval import IntervalHelper
from db_models import (
    TelegramQuiz,
    TelegramQuizAnswer,
    TelegramQuizAnswered,
    TelegramInlineButton,
    TelegramUser,
)
from custom_filter import inline_button_filter

jobstores = {"default": SQLAlchemyJobStore(url="sqlite:///jobs")}
scheduler = BackgroundScheduler(jobstores=jobstores)
scheduler.start()


# create quiz using /create_quiz Die Frage {1d} | Richtig {r} | Falsch
@Client.on_message(filters.command(["create_quiz", "create_tquiz"]))
def create_quiz(client, message):
    for admin in client.iter_chat_members("asozial", filter="administrators"):
        if admin.can_post_messages and admin.user.id == message.from_user.id:
            command = message.text.markdown.replace("/create_quiz", "").split("|")

            duration = IntervalHelper(
                re.findall("{.+}", command[0])[0][1:-1]
            ).to_timedelta()
            quiz = TelegramQuiz.create(
                question=command[0].replace(re.findall("{.+}", command[0])[0], ""),
                ends=datetime.now() + duration,
            )
            keyboard = []
            for answer in command[1:]:
                answerdb = TelegramQuizAnswer.create(
                    quiz=quiz,
                    answer=answer.replace("{r}", "").replace("{f}", "").strip(),
                    correct=True if "{r}" in answer else False,
                    answer_id=str(uuid4()),
                )
                cbdata = TelegramInlineButton.create(
                    button_id=str(uuid4()), data=f"ch:qz:{answerdb.answer_id}"
                )
                keyboard.append(
                    [
                        InlineKeyboardButton(
                            answerdb.answer, callback_data=cbdata.button_id
                        )
                    ]
                )

            client.send_message(
                chat_id="asozial"
                if message.text.startswith("/create_quiz")
                else "asozialt",
                text=quiz.question,
                reply_markup=InlineKeyboardMarkup(keyboard),
            )
            message.reply_text(
                f"Das ergebnis kannst du mit /evaluate_quiz {quiz._pk} bekommen."
            )


@Client.on_message(filters.command("evaluate_quiz"))
def evaluate_quiz(client, message):
    for admin in client.iter_chat_members("asozial", filter="administrators"):
        if admin.can_post_messages and admin.user.id == message.from_user.id:
            quiz = TelegramQuiz.get_by_id(message.command[1])
            answers = TelegramQuizAnswer.select().where(TelegramQuizAnswer.quiz == quiz)
            right = 0
            wrong = 0
            for answer in TelegramQuizAnswered.select().where(
                TelegramQuizAnswered.answer in answers
            ):
                if answer.answer in answers:
                    if answer.answer.correct:
                        right += 1
                    else:
                        wrong += 1
            if not right + wrong:
                message.reply_text(
                    f"Beim Quiz: \n\n"
                    f"{quiz.question}\n\n"
                    f"gab es (noch) keine antworten. \n"
                )
            else:
                message.reply_text(
                    text=f"Beim Quiz: \n\n"
                    f"{quiz.question}\n\n"
                    f"gab es \n"
                    f"{right+wrong} antworten, davon \n"
                    f"{right} ({round((right/(wrong+right))*100,2)}%) richtige antworten \n"
                    f"{wrong} ({round((wrong/(wrong+right))*100,2)}%) falsche antworten \n",
                )


@Client.on_callback_query(inline_button_filter("ch:qz"))
def callback_query(client, callback_query):
    data = TelegramInlineButton.get(button_id=callback_query.data).data.replace(
        "ch:qz:", ""
    )
    if callback_query.from_user.id not in [
        user.user_id for user in TelegramUser.select()
    ]:
        TelegramUser.create(user_id=callback_query.from_user.id)
    user = TelegramUser.get(user_id=callback_query.from_user.id)
    quiz = TelegramQuizAnswer.get(answer_id=data).quiz
    if quiz.ends < datetime.now():
        callback_query.answer("Zu spät, das quiz ist schon vorbei")
        return
    quizanswers = TelegramQuizAnswer.select().where(TelegramQuizAnswer.quiz == quiz)
    has_answered = False
    for quizanswer in quizanswers:
        for answered in TelegramQuizAnswered.select().where(
            TelegramQuizAnswered.answer == quizanswer
        ):
            if answered.user == user:
                callback_query.answer(
                    f'Nö, du hast für "{answered.answer.answer}" gestimmt.'
                )
                has_answered = True
    if not has_answered:
        TelegramQuizAnswered.create(
            user=TelegramUser.get(user_id=callback_query.from_user.id),
            answer=TelegramQuizAnswer.get(answer_id=data),
        )
        callback_query.answer("Super. hab ich gespeichert")
