from uuid import uuid4
from datetime import datetime
from peewee import *

db = SqliteDatabase(f"an.db")
db.connect()


class BaseModel(Model):
    class Meta:
        database = db


class TelegramGroup(BaseModel):
    group_id = IntegerField(unique=True)
    welcome = CharField(null=True)
    fun_enabled = BooleanField(default=True)
    gm_enabled = BooleanField(default=True)


if not TelegramGroup.table_exists():
    TelegramGroup.create_table()


class DiscordChannel(BaseModel):
    guild = IntegerField()
    channel = IntegerField()
    webhook_id = IntegerField()
    webhook_token = CharField()


if not DiscordChannel.table_exists():
    DiscordChannel.create_table()

# TODO:
# class MatrixGroup(BaseModel):
#    pass


class Bridge(BaseModel):
    telegram = ForeignKeyField(TelegramGroup, null=True, default=None)
    discord = ForeignKeyField(DiscordChannel, null=True, default=None)
    # TODO: matrix = ForeignKeyField(MatrixGroup, null=True, default=None)


if not Bridge.table_exists():
    Bridge.create_table()


class TelegramContactGroup(Model):
    name = CharField(unique=True)
    description = CharField()
    group = ForeignKeyField(TelegramGroup, unique=True)
    anonymous = BooleanField(default=True)

    class Meta:
        database = db


if not TelegramContactGroup.table_exists():
    TelegramContactGroup.create_table()


class TelegramUser(Model):
    user_id = IntegerField(unique=True)
    title = CharField(null=True)

    class Meta:
        database = db


if not TelegramUser.table_exists():
    TelegramUser.create_table()


class TelegramQuiz(Model):
    question = CharField()
    ends = DateTimeField()

    class Meta:
        database = db


if not TelegramQuiz.table_exists():
    TelegramQuiz.create_table()


class TelegramQuizAnswer(Model):
    quiz = ForeignKeyField(TelegramQuiz)
    answer = CharField()
    correct = BooleanField()
    answer_id = CharField(unique=True)

    class Meta:
        database = db


if not TelegramQuizAnswer.table_exists():
    TelegramQuizAnswer.create_table()


class TelegramQuizAnswered(Model):
    user = ForeignKeyField(TelegramUser)
    answer = ForeignKeyField(TelegramQuizAnswer)

    class Meta:
        database = db


if not TelegramQuizAnswered.table_exists():
    TelegramQuizAnswered.create_table()


class TelegramInlineButton(Model):
    button_id = CharField(unique=True)
    data = CharField(default=lambda: str(uuid4()))
    created = DateTimeField(default=datetime.now)

    class Meta:
        database = db


if not TelegramInlineButton.create_table():
    TelegramInlineButton.create_table()
