from datetime import datetime, timedelta
from pyrogram import filters
from db_models import TelegramInlineButton, TelegramGroup


def inline_button_filter(data):
    def custom_filter(flt, client, query):
        for button in TelegramInlineButton.select():
            if button.created+timedelta(days=5) < datetime.now():
                button.delete_instance()
        return TelegramInlineButton.get(button_id=query.data).data.startswith(flt.data)

    return filters.create(custom_filter, data=data)


group_allowed = filters.create(
    lambda fltr, client, update: update.chat.id
    in [group.group_id for group in TelegramGroup.select()]
)
