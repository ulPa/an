#!venv/bin/python
from configparser import ConfigParser
import pyrogram
import discord
from db_models import Bridge

config = ConfigParser()
config.read("config.ini")
discord_token = config["discord"]["token"]

tclient = pyrogram.Client("discord", config_file="config.ini")
tclient.start()
dclient = discord.Client()


@dclient.event
async def on_message(message):
    for bridge in Bridge.select():
        if bridge.discord and bridge.telegram:
            if bridge.discord.channel == message.channel.id:
                if not message.author.display_name.endswith("[TG]"):
                    if ('jo' not in message.content.lower()) or (len(message.content) > 5):
                        await tclient.send_message(
                            text=f"**{message.author.display_name}**\n{message.content}",
                            chat_id=bridge.telegram.group_id,
                        )


dclient.run(discord_token)
