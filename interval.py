# This class was created for combot and has been disclosed by Sergey and edited a tiny bit by me(ulPa <gitlab.com/ulPa>)
# https://t.me/combotchat/45392

import re
from datetime import timedelta


class IntervalHelper(object):
    class IntervalError(Exception):
        pass

    interval_re = re.compile(r"^(\d+)([smhdw])?$")

    def __init__(self, _interval):
        self._interval = _interval
        self._intervals = _interval.split(" ")
        print(self._intervals)

    def to_timedelta(self):
        time = timedelta()
        for intrvl in self._intervals:
            m = IntervalHelper.interval_re.match(intrvl)
            num, unit = m.groups()

            num = int(num)

            if not unit:
                unit = "m"

            if unit == "s":
                time += timedelta(seconds=num)
            elif unit == "m":
                time += timedelta(minutes=num)
            elif unit == "h":
                time += timedelta(hours=num)
            elif unit == "d":
                time += timedelta(days=num)
            elif unit == "w":
                time += timedelta(weeks=num)
        return time

    interval = property(lambda self: self._interval)


if __name__ == "__main__":
    print(IntervalHelper("1h").to_timedelta())
    print(IntervalHelper("1h 30m").to_timedelta())
